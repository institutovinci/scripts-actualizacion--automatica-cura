REM Este script crea una nueva tarea programada de Windows. La tarea ejecuta el script de 
REM actualizar los perfiles de Cura cada vez que se inicia la máquina.
REM 

SCHTASKS /Create /TN actualizar_perfiles_cura /SC onlogon /F /TR "%windir%\system32\cmd.exe /c 'start /min %UserProfile%\Documents\Vinci\actualizar_perfiles_cura.bat'"

exit