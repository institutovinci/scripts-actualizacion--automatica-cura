REM @echo off
REM Este programa actualiza las carpetas de configuración de Cura. Clona el
REM repositorio de Gitlab si no existe ya, lo actualiza y copia las carpetas.
REM Pablo Aguado 2018 - Instituto Vinci

REM Carpeta donde están las carpetas de configuración de Cura. Ruta absoluta.
REM set CARPETA_CURA="%UserProfile%\Desktop\pruebagit"
set CARPETA_CURA="%UserProfile%\AppData\Roaming\cura"

cd %CARPETA_CURA%

REM Si no existe repositorio local
IF NOT EXIST ".\tempgit\.git\" (

echo ---No existe el repositorio local, así que se va a clonar el de Gitlab.

REM Clonamos en temporal
rmdir tempgit
git clone https://gitlab.com/institutovinci/perfiles-cura .\tempgit
goto ACTUALIZAR

) ELSE (
echo ---Ya existe un repositorio local, así que se va a actualizar.

REM Hay repo. Actualizamos y sobreescribimos.
cd tempgit
git fetch --all
git reset --hard origin/master
cd..
goto :actualizar
)

:ACTUALIZAR
REM Copiamos todo a carpeta de configuración
xcopy tempgit .\ /E /Y

REM Listo.

exit